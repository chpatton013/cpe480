import random

class PrisonerAgent:

   def __init__(self, cooperateRate):
      if (cooperateRate < 0 or cooperateRate > 1):
         raise ValueError("cooperateRate must be between 0 and 1")
      self.cooperateRate = cooperateRate

   def play(self):
      if (random.random() < self.cooperateRate):
         return "C"
      else:
         return "D"

   def playStats(self, n):
      didCooperate = lambda _, __: 1 if (self.play() == "C") else 0
      return reduce(didCooperate, range(1, n), 0) / n
