def Chris(hist = [], score = [], query = False):
   if query:
      name = "The Collaborator"
      motto = "Wash, tell me I'm pretty."
      return [name, motto]

   establishPeriod = 50
   assessmentPeriod = 50

   if (len(hist) < establishPeriod):
      return 'C'

   def calculateHistoricalScores(hist, assessmentPeriod):
      def getScores(move1, move2):
         payoffMatrix = {
            'C': {
               'C': (3, 3),
               'D': (0, 5)
            },
            'D': {
               'C': (5, 0),
               'D': (1, 1)
            },
         }

         return payoffMatrix[move1][move2]

      score1 = 0
      score2 = 0

      for (move1, move2) in hist[-assessmentPeriod:]:
         scores = getScores(move1, move2)
         score1 = score1 + scores[0]
         score2 = score2 + scores[1]

      return (score1, score2)

   scores = calculateHistoricalScores(hist, assessmentPeriod)
   myScore = scores[0]
   theirScore = scores[1]

   if (myScore < theirScore):
      return 'D'

   return 'C'
