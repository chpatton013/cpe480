def Patton(hist = [], score = [], query = False):
   if query:
      name = "Serenity"
      motto = "Wash, tell me I'm pretty."
      return [name, motto]

   establishPeriod = 50
   assessmentPeriod = 50

   if (len(hist) < establishPeriod):
      return 'C'

   def reduceHistory(hist, assessmentPeriod):
      reduction1 = 0
      reduction2 = 0

      for (move1, move2) in hist[-assessmentPeriod:]:
         if (move1 == 'C'):
            reduction1 = reduction1 + 1
         if (move2 == 'C'):
            reduction2 = reduction2 + 1

      return (reduction1, reduction2)

   reduction = reduceHistory(hist, assessmentPeriod)
   if (reduction[0] > reduction[1]):
      return 'D'

   return 'C'
