import random;

def roundRobin(agentDB, matchRounds = 1000, F = 0.5, I = 0.001, verbose = 0):
	def getMove(agent, hist, score, flipChance):
		flip = (random.random() < flipChance)
		move = eval('{0}(hist, score)'.format(agent))
		if flip:
			if move == 'C':
				return 'D'
			else:
				return 'C'
		else:
			return move

	def getScores(move1, move2):
		payoffMatrix = {
			'C': {
				'C': (3, 3),
				'D': (0, 5)
			},
			'D': {
				'C': (5, 0),
				'D': (1, 1)
			},
		}

		return payoffMatrix[move1][move2]

	def query(agent):
		return eval('{0}(query=True)'.format(agent))

	scores = {}
	for agent in agentDB:
		scores[agent] = 0

	currentAgents = agentDB[:]
	numAgents = len(currentAgents)
	while numAgents > 1:
		for index1 in xrange(0, numAgents):
			for index2 in xrange(index1 + 1, numAgents):
				agent1 = currentAgents[index1]
				agent2 = currentAgents[index2]

				if verbose > 1:
					print('Match {0} vs {1}'.format(agent1, agent2))

				score1 = 0
				score2 = 0

				hist1 = []
				hist2 = []

				for iteration in xrange(0, matchRounds):
					flipChance = max(0, F - I * iteration)

					move1 = getMove(agent1, hist1, score1, flipChance)
					move2 = getMove(agent2, hist2, score2, flipChance)

					score = getScores(move1, move2)

					score1 = score1 + score[0]
					score2 = score2 + score[1]

					hist1.append((move1, move2))
					hist2.append((move2, move1))

					if verbose > 2:
						print('Turn results')
						print('{0}: {1}: {2}'.format(agent1, move1, score1))
						print('{0}: {1}: {2}'.format(agent2, move2, score2))
						print('')

				scores[agent1] = scores[agent1] + score1
				scores[agent2] = scores[agent2] + score2

				if verbose > 1:
					print('Match results')
					print('{0}: {1}'.format(agent1, score1))
					print('{0}: {1}'.format(agent2, score2))
					print('')

		if verbose > 0:
			print('Round results')
			for agent in currentAgents:
				agentInfo = query(agent)
				name = agentInfo[0]
				score = scores[agent]
				print('{0}: {1}'.format(name, score))
			print('')

		worstAgent = currentAgents[0]
		for agent in currentAgents:
			if (scores[agent] < scores[worstAgent]):
				worstAgent = agent
		currentAgents.remove(worstAgent)
		numAgents = len(currentAgents)

	for agent in sorted(scores, key = scores.get, reverse = True):
		agentInfo = query(agent)
		name = agentInfo[0]
		motto = agentInfo[1]
		print('{0}: {1}: {2}'.format(name, motto, scores[agent]))
