import sys

class Direction:
   up = 0
   down = 1
   left = 2
   right = 3

class Movement:
   forward = 0
   left = 1
   right = 2

class Terrain:
   normal = 0
   wall = 1
   portal = 2

class Agent:
   def __init__(self):
      self.positionX = 0
      self.positionY = 0
      self.direction = Direction.right

   def positionString(self):
      return '({x},{y})'.format(x=self.positionX, y=self.positionY)

   def directionString(self):
      if (self.direction == Direction.up):
         return 'up'
      elif (self.direction == Direction.down):
         return 'down'
      elif (self.direction == Direction.left):
         return 'left'
      elif (self.direction == Direction.right):
         return 'right'

   def move(self, movement, environment):
      if (movement == Movement.left):
         if (self.direction == Direction.up):
            self.direction = Direction.left
         elif (self.direction == Direction.down):
            self.direction = Direction.right
         elif (self.direction == Direction.left):
            self.direction = Direction.down
         elif (self.direction == Direction.right):
            self.direction = Direction.up

      elif (movement == Movement.right):
         if (self.direction == Direction.up):
            self.direction = Direction.right
         elif (self.direction == Direction.down):
            self.direction = Direction.left
         elif (self.direction == Direction.left):
            self.direction = Direction.up
         elif (self.direction == Direction.right):
            self.direction = Direction.down

      elif (movement == Movement.forward):
         if (self.direction == Direction.up):
            if (self.positionY > 0 and (environment[self.positionX][self.positionY - 1] != Terrain.wall)):
               self.positionY = self.positionY - 1
         elif (self.direction == Direction.down):
            if (self.positionY < 9 and (environment[self.positionX][self.positionY + 1] != Terrain.wall)):
               self.positionY = self.positionY + 1
         elif (self.direction == Direction.left):
            if (self.positionX > 0 and (environment[self.positionX - 1][self.positionY] != Terrain.wall)):
               self.positionX = self.positionX - 1
         elif (self.direction == Direction.right):
            if (self.positionX < 9 and (environment[self.positionX + 1][self.positionY] != Terrain.wall)):
               self.positionX = self.positionX + 1

         if (environment[self.positionX][self.positionY] == Terrain.portal):
            self.positionX = 0
            self.positionY = 0

def parseFile(fileName):
   f = open(fileName, 'r')

   x = 0
   y = 0
   environment = [[Terrain.normal for _ in xrange(10)] for _ in xrange(10)]
   commands = []

   for line in f:
      line = line.strip()

      if (line == ''):
         continue

      if (line == 'forward'):
         commands.append(Movement.forward)
      elif (line == 'left'):
         commands.append(Movement.left)
      elif (line == 'right'):
         commands.append(Movement.right)
      else:
         for space in str.split(line):
            space = space.strip()

            if (space == '0'):
               environment[x][y] = Terrain.normal
            elif (space == '1'):
               environment[x][y] = Terrain.wall
            elif (space == '2'):
               environment[x][y] = Terrain.portal

            x = (x + 1) % 10

         y = (y + 1) % 10

   return (environment, commands)

fileName = sys.argv[1]

tup = parseFile(fileName)
environment = tup[0]
commands = tup[1]

agent = Agent()
for command in commands:
   agent.move(command, environment)

print(agent.positionString())
