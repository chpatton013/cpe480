#!/usr/bin/env python3

elements = [
   'city',
   'state',
   'estimate',
   'census',
   'change',
   'land area',
   'population density',
   'latitude',
   'longitude',
]

labels = {
   'city': 'City',
   'state': 'State',
   'estimate': '2013 Population Estimate',
   'census': '2010 Census',
   'change': 'Population Change (%)',
   'land area': 'Land Area (sq mi)',
   'population density': 'Population Density (per sq mi)',
   'latitude': 'Latitude (deg N)',
   'longitude': 'Longitude (deg W)',
}

cities = {
   'New York': {
      'city': 'New York',
      'state': 'New York',
      'estimate': '8405837',
      'census': '8175133',
      'change': '2.82',
      'land area': '302.6',
      'population density': '27012',
      'latitude': '40.6643',
      'longitude': '73.9385',
   },
   'Los Angeles': {
      'city': 'Los Angeles',
      'state': 'California',
      'estimate': '3884307',
      'census': '3792621',
      'change': '2.42',
      'land area': '468.7',
      'population density': '8092',
      'latitude': '34.0194',
      'longitude': '118.4108',
   },
   'Chicago': {
      'city': 'Chicago',
      'state': 'Illinois',
      'estimate': '2718782',
      'census': '2695598',
      'change': '0.86',
      'land area': '227.6',
      'population density': '11842',
      'latitude': '41.8376',
      'longitude': '87.6818',
   },
   'Houston': {
      'city': 'Houston',
      'state': 'Texas',
      'estimate': '2195914',
      'census': '2100263',
      'change': '4.55',
      'land area': '599.6',
      'population density': '3501',
      'latitude': '29.7805',
      'longitude': '95.3863',
   },
   'Philadelphia': {
      'city': 'Philadelphia',
      'state': 'Pennsylvania',
      'estimate': '1553165',
      'census': '1526006',
      'change': '1.78',
      'land area': '134.1',
      'population density': '11379',
      'latitude': '40.0094',
      'longitude': '75.1333',
   },
   'Phoenix': {
      'city': 'Phoeniz',
      'state': 'Arizona',
      'estimate': '1513367',
      'census': '1445632',
      'change': '4.69',
      'land area': '516.7',
      'population density': '1338.3',
      'latitude': '33.5722',
      'longitude': '112.0880',
   },
   'San Antonio': {
      'city': 'San Antonio',
      'state': 'Texas',
      'estimate': '1409019',
      'census': '1327407',
      'change': '6.15',
      'land area': '460.9',
      'population density': '2880',
      'latitude': '29.4724',
      'longitude': '98.5251',
   },
   'San Diego': {
      'city': 'San Diego',
      'state': 'California',
      'estimate': '1355896',
      'census': '1307402',
      'change': '3.71',
      'land area': '325.2',
      'population density': '4020',
      'latitude': '32.8153',
      'longitude': '117.1350',
   },
   'Dallas': {
      'city': 'Dallas',
      'state': 'Texas',
      'estimate': '127676',
      'census': '1197816',
      'change': '5.00',
      'land area': '340.5',
      'population density': '3518',
      'latitude': '32.7757',
      'longitude': '96.7967',
   },
   'San Jose': {
      'city': 'San Jose',
      'state': 'California',
      'estimate': '998537',
      'census': '945942',
      'change': '5.56',
      'land area': '176.5',
      'population density': '5359',
      'latitude': '37.2969',
      'longitude': '121.8193',
   },
}

topCities = map(lambda city: city[0], sorted(cities.items(), key=lambda city: int(city[1]['estimate']), reverse=True))

padding = dict(map(lambda (element, string): (element, len(string)), labels.iteritems()))
for city in topCities:
   for element, value in cities[city].iteritems():
      padding[element] = max(len(value), padding[element])

table = ''

for element in elements:
   table += '+-' + ('-' * padding[element]) + '-'
table += '+\n'

for element in elements:
   elementPadding = padding[element] - len(labels[element])
   table += '| ' + labels[element] + (' ' * elementPadding) + ' '
table += '|\n'

for element in elements:
   table += '+-' + ('-' * padding[element]) + '-'
table += '+\n'

for city in topCities:
   for element in elements:
      elementPadding = padding[element] - len(cities[city][element])
      table += '| ' + cities[city][element] + (' ' * elementPadding) + ' '
   table += '|\n'

for element in elements:
   table += '+-' + ('-' * padding[element]) + '-'
table += '+'

print(table)
