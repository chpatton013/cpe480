1.8)  The end goal of machines doing all this complicated math is to make sense
      of the visual imput they are receiving. They have to process it into a
      usable form (generally a matrix of numerical values) so that they can
      incorporate it into different software algorithms.
      Humans, on the other hand, don't have to do any software algorithms. Our
      brains are essentially very advanced pieces of hardware that have been
      crafted over millenia of evolution to make sense of visual input.

1.9)  Rational systems are ones that do the "right thing". In the context of
      biological systems, the right thing is what contributes to the most
      successfully reproducing organisms. In order to reproduce, the organisms
      must live long enough to mate, gestate, and raise young. Survival over
      this time frame almost always requires the organism to make choices.
      Natural selection favors organisms that make rational choices by rewarding
      them with more opportunities to reproduce. So by our definition of
      "rational", evolution must reward rational systems.

1.10) Science is the process of repeated experimentation to discover truth.
      Engineering is the process of using truths learned by science to build a
      system. While AI has science to back it, it is still a system which has to
      be engineered.

2.1)  Any action that can be taken by an agent can have an expected cost and
      expected payoff. The expected value of any action is the difference of the
      payoff and the cost. Because very few components of the environment can be
      deterministic, there is always some variance in the expected value.
      If after the first T time steps a safe option (lowest cost with variance
      considered) can be taken, a rational agent should take this option over
      one with the highest expected value.

2.2a) Assumptions:
      ------------
       * The performance measure awards one point for each clean square at each
         time step, over a "lifetime" of 1000 time steps.
       * The "geography" of the environment is known a priori (Figure 2.2) but
         the dirt distribution and the initial location of the agent are not.
         Clean squares stay clean and sucking cleans the current square. The
         Left and Right actions move the agent left and right except when this
         would take the agent outside the environment, in which case the agent
         remains where it is.
       * The only available actions are Left, Right, and Suck.
       * The agent correctly perceives its location and whether that location
         contains dirt.
      ------------
      Environment:
      ------------
      +---+---+
      | A | B |
      +---+---+
      -----------------
      Percept Sequence:
      -----------------
      +-------------------------------------+--------+
      | Percept                             | Action |
      +-------------------------------------+--------+
      | [A, Clean]                          | Right  |
      | [A, Dirty]                          | Suck   |
      | [B, Clean]                          | Left   |
      | [B, Dirty]                          | Suck   |
      | [A, Clean], [A, Clean]              | Right  |
      | [A, Clean], [A, Dirty]              | Suck   |
      | [A, Clean], [A, Clean], [A, Clean]  | Right  |
      | [A, Clean], [A, Clean], [A, Dirty]  | Suck   |
      +-------------------------------------+--------+
      -------
      Answer:
      -------
      Because points are awarded over a series of time steps and more points are
      given when more spaces are clean, it is optimal to clean the entire
      environment as quickly as possible. The example environment creates a
      discrete set of states, each with a single optimal strategy for cleaning
      in minimum time.
      In English, the optimal strategy is as follows:
       * If space is dirty, suck.
       * Move to other space.
       * If space is dirty, suck.
      The follwoing cases are possible:
       1. A Clean, B Clean, Start A
       2. A Clean, B Clean, Start B
       3. A Clean, B Dirty, Start A
       4. A Clean, B Dirty, Start B
       5. A Dirty, B Clean, Start A
       6. A Dirty, B Clean, Start B
       7. A Dirty, B Dirty, Start A
       8. A Dirty, B Dirty, Start B
      The optimal strategies for each case are:
       1. Right
       2. Left
       3. Right, Suck
       4. Suck, Left
       5. Suck, Right
       6. Left, Suck
       7. Suck, Left, Suck
       8. Suck, Right, Suck
      The provided percept sequence mirrors these strategies exactly, making
      this a rational agent.

2.2b) A rational agent would require internal state if the environment has
      arbitrary geography. Without arbitrary geography, the game stays the same:
      clean the world in as few steps as possible. With arbitrary geography it
      is necessary to utilize internal state to "remember" the geography that
      has been visited and find an ideal path to search the entirety of it.

2.2c) The best way to maximize score when clean spaces can become dirty is to
      move the agent in a patrol through every space in the environment. The
      agent would have to learn the geography of the environment and identify
      the optimal path it could take to get into and maintain its patrol.

2.3)  This problem is too long, so I'm not doing it.

2.4)  This one is too long also, but I got too far into it to give up now.
       * Playing soccer.
         P: Maxmize team score
         E: Soccer field, team players, soccer ball, nets
         A: Player legs, head, chest, and hands (if keeper)
         S: Player eyes
       * Exploring the subsurface oceans of Titan.
         P: Maxmize detail gathered x area covered
         E: Telescope housing
         A: Positioning/rotation and focus on telescope
         S: Telescope
       * Shopping for used AI books on the Internet.
         P: Maxmize book quality, minimize book cost
         E: Computer/peripherals, amazon marketplace (who shops anywhere else?)
         A: Keyboard buttons, mouse position, mouse buttons
         S: Shopper eyes
       * Playing a tennis match.
         P: Win 3 sets
         E: Tennis court, player, opponent, tennis ball, net
         A: Player position, racket
         S: Player eyes, game score
       * Practicing tennis against a wall.
         P: That depends on why you are practicing
         E: Half "court", player, tennis ball
         A: Player position, racket
         S: Player eyes
       * Performing a high jump.
         P: Maximize vertical height
         E: Bar, landing pad
         A: Player legs, body shape
         S: Bar height
       * Knitting a sweater.
         P: Quality of sweater
         E: Sweater
         A: Needles
         S: Knitter's perception of sweater quality and completeness
       * Bidding on an item at an auction.
         P: Minimize price
         E: Auction
         A: Bid paddle
         S: Actioneer's price

3.1)  Problem formulation is "the process of deciding what actions and states to
      consider, given a goal", so a goal is needed (AKA, it is necessary to
      complete goal formulation) before starting problem formulation.

3.2)  Too long.

3.3)  Noooooope.

3.4)  Nope nope nope.

3.25) Wat?
