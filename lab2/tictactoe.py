def abSearch(state):
   action, value = abMaxValue(state, float('-inf'), float('inf'), 0)
   return action

def abMaxValue(state, alpha, beta, depth):
   if state.isTerminal():
      return (None, state.utility(depth))

   value = float('-inf')
   maxAction = None
   for (action, s) in state.successors(1):
      minAction, minValue = abMinValue(s, alpha, beta, depth + 1)
      if minValue > value:
         maxAction = minAction
         value = minValue

      if value >= beta:
         return (action, value)

      if alpha < value:
         alpha = value
         maxAction = action

   return (maxAction, value)

def abMinValue(state, alpha, beta, depth):
   if state.isTerminal():
      return (None, state.utility(depth))

   value = float('inf')
   minAction = None
   for (action, s) in state.successors(2):
      maxAction, maxValue = abMaxValue(s, alpha, beta, depth + 1)
      if maxValue < value:
         minAction = maxAction
         value = maxValue

      if value <= alpha:
         return (minAction, value)

      if beta > value:
         beta = value
         minAction = action

   return (minAction, value)

class TicTacToeState:
   def __init__(self, board = None):
      if board == None:
         self.board = [[0 for _ in xrange(3)] for _ in xrange(3)]
      else:
         self.board = board

   def isTerminal(self):
      one = [1,1,1]
      two = [2,2,2]

      # Rows
      if one in self.board or two in self.board:
         return True

      # Columns
      for col in xrange(3):
         sequence = [0,0,0]
         for row in xrange(3):
            sequence[row] = self.board[row][col]

         if sequence == one or sequence == two:
            return True

      # Diagonals
      sequence = [0,0,0]

      for index in xrange(3):
         sequence[index] = self.board[index][index]

      if sequence == one or sequence == two:
         return True

      sequence = [0,0,0]

      for index in xrange(3):
         sequence[index] = self.board[index][2 - index]

      if sequence == one or sequence == two:
         return True

      # Tie
      for row in self.board:
         if 0 in row:
            return False

      return True

   def utility(self, depth):
      scale = 9 - depth

      one = [1,1,1]
      two = [2,2,2]

      # Rows
      if one in self.board:
         return scale
      if two in self.board:
         return -scale

      # Columns
      for col in xrange(3):
         sequence = [0,0,0]
         for row in xrange(3):
            sequence[row] = self.board[row][col]

         if sequence == one:
            return scale
         if sequence == two:
            return -scale

      # Diagonals
      sequence = [0,0,0]

      for index in xrange(3):
         sequence[index] = self.board[index][index]

      if sequence == one:
         return scale
      if sequence == two:
         return -scale

      sequence = [0,0,0]

      for index in xrange(3):
         sequence[index] = self.board[index][2 - index]

      if sequence == one:
         return scale
      if sequence == two:
         return -scale

      # Tie
      return 0

   def successors(self, turn):
      possibilities = []

      for row in xrange(3):
         for col in xrange(3):
            if self.board[row][col] == 0:
               action = (row, col)

               board = [[0 for _ in xrange(3)] for _ in xrange(3)]
               for x in xrange(3):
                  for y in xrange(3):
                     board[x][y] = self.board[x][y]
               board[row][col] = turn
               state = TicTacToeState(board)

               possibilities.append((action, state))

      return possibilities

class Play:
   def __init__(self):
      self.state = TicTacToeState()

   def getMoveStr(self, value):
      if value == 1:
         return 'O'
      elif value == 2:
         return 'X'
      else:
         return '_'

   def printBoard(self):
      for row in xrange(3):
         print('{0} {1} {2}'.format(
          self.getMoveStr(self.state.board[row][0]),
          self.getMoveStr(self.state.board[row][1]),
          self.getMoveStr(self.state.board[row][2])))

   def printMove(self, action, turn):
      row, col = action
      if turn == 1:
         print('Opponent choosing ({0}, {1})'.format(row + 1, col + 1))
      else:
         print('Choosing ({0}, {1})'.format(row + 1, col + 1))

   def prompt(self):
      self.printBoard()
      index = int(raw_input('Pick an empty spot on the board [1-9]: ')) - 1
      row = int(index / 3)
      col = index % 3

      return (row, col)

   def playHuman(self):
      row, col = self.prompt()
      while row < 0 or row > 2 or col < 0 or col > 2 or self.state.board[row][col] != 0:
         row, col = self.prompt()
      self.state.board[row][col] = 2

      print('')
      self.printMove((row,col), 2)

   def playRobot(self):
      row, col = abSearch(self.state)
      self.state.board[row][col] = 1

      self.printMove((row,col), 1)
      print('')

   def play(self):
      turn = 2
      while not self.state.isTerminal():
         if turn == 1:
            self.playRobot()
            turn = 2
         else:
            self.playHuman()
            turn = 1

      self.printBoard()
      print('')

      score = self.state.utility(0)
      if score < 0:
         print('You won! :D')
      elif score > 0:
         print('You lost! :(')
      else:
         print('It\'s a tie!')

Play().play()
