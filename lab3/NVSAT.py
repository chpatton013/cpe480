import sys
import string

if len(sys.argv) != 2:
   print('usage: python NVSAT.py <filename>')
   sys.exit(1)

filename = sys.argv[1]

def xor(a, b):
   return (a or b) and not (a and b)

def getClauses(filename):
   f = open(filename, 'r')

   conjunctive = []
   for line in f:
      varList = line.rstrip().split(' ')

      subjunctive = []
      for var in varList:
         if var == '-':
            subjunctive.append(None)
         elif var == '0':
            subjunctive.append(False)
         elif var == '1':
            subjunctive.append(True)
         else:
            break

      if len(subjunctive) > 0:
         conjunctive.append(subjunctive)

   return conjunctive

def printAnswer(answer):
   if answer == None:
      print 'unsatisfiable'
      return

   accumulator = []
   for index in xrange(len(answer)):
      value = answer[index]
      if value != None:
         accumulator.append('X{0} = {1}'.format(index + 1, value))

   print(', '.join(accumulator))

def countOccurences(row):
   count = {True: 0, False: 0, None: 0}
   for value in row:
      count[value] = count[value] + 1

   return count

def getSingleCandidateNonTrivial(row):
   rowSet = set(row)
   value = rowSet.pop()
   index = row.index(value)
   return (index, value)

def reduceConstraints(constraints):
   def isSingleCandidateRow(row):
      count = countOccurences(row)
      return (count[None] == len(row) - 1) and xor(count[True] == 1, count[False] == 1)

   def removeDuplicates(constraints):
      constraintSet = []
      for row in constraints:
         if row not in constraintSet:
            constraintSet.append(row)
      return constraintSet

   noneSet = set([None])
   changed = False

   for i in xrange(len(constraints)):
      row = constraints[i]

      if not isSingleCandidateRow(row):
         continue;

      nonTrivialIndex, nonTrivialValue = getSingleCandidateNonTrivial(row)

      for j in xrange(len(constraints)):
         if i == j:
            continue

         otherRow = constraints[j]
         otherNonTrivialValue = otherRow[nonTrivialIndex]
         if nonTrivialValue == otherNonTrivialValue:
            for k in xrange(len(otherRow)):
               if k == nonTrivialIndex:
                  continue
               if otherRow[k] == None:
                  continue
               constraints[j][k] = None
               changed = True
         else:
            if isSingleCandidateRow(otherRow):
               return None
            if otherRow[nonTrivialIndex] != None:
               constraints[j][nonTrivialIndex] = None
               changed = True

   if not changed:
      return removeDuplicates(constraints)

   return reduceConstraints(constraints)

def getKnownColumns(constraints):
   known = {}

   for index in xrange(len(constraints[0])):
      col = [row[index] for row in constraints]
      count = countOccurences(col)

      if count[None] == len(col):
         known[index] = None
      elif (count[None] == 0) and (count[True] > 0) and (count[False] > 0):
         return None
      elif xor(count[True] == len(col), count[False] == len(col)):
         nonTrivialIndex, nonTrivialValue = getSingleCandidateNonTrivial(col)
         known[index] = nonTrivialValue

   return known

def isSatisfied(constraints, suggestion):
   def isClauseSatisfied(clause, suggestion):
      for index in xrange(len(clause)):
         if clause[index] != None and clause[index] == suggestion[index]:
            return True

      return False

   for clause in constraints:
      if not isClauseSatisfied(clause, suggestion):
         return False

   return True

def advance(suggestion, index):
   suggestionCopy = suggestion[:]
   if suggestionCopy[index] == True:
      suggestionCopy[index] = False
   return suggestionCopy

def solve(constraints):
   def r_solve(constraints, suggestion, known):
      if isSatisfied(constraints, suggestion):
         return suggestion

      for index in xrange(len(suggestion)):
         if index in known:
            continue
         if suggestion[index] == False:
            continue
         nextSuggestion = advance(suggestion, index)
         nextResult = r_solve(constraints, nextSuggestion, known)
         if nextResult != None:
            return nextResult

      return None

   constraints = reduceConstraints(constraints)
   if constraints == None:
      return None

   known = getKnownColumns(constraints)
   if known == None:
      return None

   suggestion = [True] * len(constraints[0])
   for index, value in known.iteritems():
      suggestion[index] = value

   return r_solve(constraints, suggestion, known)

constraints = getClauses(filename)
answer = solve(constraints)
printAnswer(answer)

"""
I used three strategies to reduce complexity:
   1. Identify single candidates in rows and propogate their implications to all
      other rows. If a single candidate is found:
         foreach row:
            if row[index] matches single_candidate_row[index]:
               set all other members in row to None
            elif row is also a single candidate:
               unsatisfiable
            else:
               set row[index] to None
   2. Remove all duplicate rows
   3. Identify known variables, eliminate Nones, and look for known unsatisfiability.
      Don't bother brute forcing known values. Find known by:
         foreach col:
            if col is all None:
               mark known[index] as None
            elif col is all True and False (not only one):
               unsatisfiable
            elif col is all True:
               mark known[index] as True
            elif col is all False:
               mark known[index] as False
"""
