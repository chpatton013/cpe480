import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.Arrays;
import java.awt.Point;

/**
 * The agent will explore the wumpus world in search of the gold. They have at their displosal
 * a single arrow to kill the wumpus. They can also detect using 5 senses. This being:
 *    1) Detecting a glitter: The gold will glitter
 *    2) Detecting a breeze: Breezes appear in the directly adjacent blocks to a pit.
 *    3) Detecting a stench: The wumpus will put off a stench into the adjacent squares.
 *    4) Detecting a bump: Bumps occur when the agent attempts to walk beyond the confines of
 *       the world.
 *    5) Detecting a scream: If the agent fires his/her arrow forward into the wumpus, this will
 *       effectively slay the beast, causing it to let out a piercing cry.
 *
 *    The agent has at his disposal these methods in order to determine their next move of action:
 *      *From the agent*
 *         *turnLeft(), turnRight() - turns the player in the direction indicated.
 *         *move() - advances the player one grid space forward in the direction they are currently
 *                    facing. This direction can also be checked within the private String heading,
 *                    which will indicate NORTH, EAST, WEST, or SOUTH.
 *         *senseBreeze(), senseStench(), senseGlitter() - these methods will return true if the
 *                    player is located on a space with the breeze, stench, or gold respectively.
 *         *shoot() - fires an arrow along the players current heading. If the wumpus is along said
 *                     heading, this method returns true to indicate a dead wumpus. UI will update
 *                     display a scream heard. This decrements the arrow count if available, or
 *                     returns false if out of arrows. Remaining arrows are found in arrowsLeft.
 *
 * @author Christopher McKee
 * @version W13
 */
public class Agent extends Actor
{
    private GreenfootImage foreground, eastImage, southImage, westImage, northImage;
    private TheWumpusWorld world;
    private String heading;
    private int arrowsLeft;
    public boolean isAlive;

    //Custom variables for the player to use while navigating
    private int[][] tracking;
    private boolean bumped, turning;

    private int PattonTrue = 1;
    private int PattonFalse = 0;
    private int PattonUnknown = -11;

    private boolean PattonHasGold = false;
    private boolean PattonKilledWumpus = false;
    private boolean PattonKillingWumpus = false;

    private boolean PattonKbInitialized = false;
    private boolean[][] PattonExploredKb = null;
    private int[][] PattonStenchKb = null;
    private int[][] PattonBreezeKb = null;
    private int[][] PattonPitKb = null;

    private LinkedList<String> PattonActionQueue = new LinkedList<String>();

    /* This method will be written by the student to include the logic needed by the agent.
     * As of now, it is has a very simple ai that is being used to show the use of the various
     * agent methods.
     */
    private void makeNextMove() {
       this.initKb();

       if (this.PattonHasGold) {
          return;
       }

       if (this.senseGlitter()) {
          this.world.takeGold();
          return;
       }

       int x = this.getX() - 1;
       int y = this.getY() - 1;

       this.PattonExploredKb[x][y] = true;
       this.PattonPitKb[x][y] = PattonFalse;

       if (!this.PattonKilledWumpus && !this.PattonKillingWumpus && this.senseStench()) {
          this.PattonKillingWumpus = true;
          this.PattonActionQueue.add("left");
          this.PattonActionQueue.add("shoot");
          this.PattonActionQueue.add("right");
          this.PattonActionQueue.add("shoot");
          this.PattonActionQueue.add("right");
          this.PattonActionQueue.add("shoot");
          this.PattonActionQueue.add("left");
       }
       if (this.PattonBreezeKb[x][y] == PattonUnknown) {
          this.PattonBreezeKb[x][y] = this.senseBreeze() ? PattonTrue : PattonFalse;
       }

       if (this.PattonActionQueue.size() > 0) {
          String action = this.PattonActionQueue.removeFirst();
          performAction(action);
          return;
       }

       Point choice = null;
       LinkedList<Point> frontier = this.buildFrontier();
       for (Point p : frontier) {
          if (!this.askPit(p.x, p.y)) {
             choice = p;
             break;
          }
       }

       if (choice == null) {
          choice = frontier.peek();
       }

       this.setPathToPoint(choice);
    }

    private void initKb() {
       if (this.PattonKbInitialized) {
          return;
       }

       int width = this.world.getPlayWidth();
       this.PattonExploredKb = new boolean[width][width];
       this.PattonStenchKb = new int[width][width];
       this.PattonBreezeKb = new int[width][width];
       this.PattonPitKb = new int[width][width];
       for (int i = 0; i < width; ++i) {
          for (int j = 0; j < width; ++j) {
             this.PattonExploredKb[i][j] = false;
             this.PattonStenchKb[i][j] = PattonUnknown;
             this.PattonBreezeKb[i][j] = PattonUnknown;
             this.PattonPitKb[i][j] = PattonUnknown;
          }
       }

       this.PattonKbInitialized = true;
    }

    private void performAction(String action) {
       if (action.equals("left")) {
          this.turnLeft();
       } else if (action.equals("right")) {
          this.turnRight();
       } else if (action.equals("move")) {
          this.move();
       } else if (action.equals("shoot") && !this.PattonKilledWumpus) {
          this.PattonKilledWumpus = this.shoot();
       }
    }

    private LinkedList<Point> buildFrontier() {
       int width = this.world.getPlayWidth();

       LinkedList<Point> frontier = new LinkedList<Point>();
       for (int x = 0; x < width; ++x) {
          for (int y = 0; y < width; ++y) {
             if (!this.PattonExploredKb[x][y]) {
                continue;
             }

             if (x > 0) {
                Point p = new Point(x - 1, y);
                if (!this.PattonExploredKb[x - 1][y] && !frontier.contains(p)) {
                   frontier.add(p);
                }
             }
             if (x < width - 1) {
                Point p = new Point(x + 1, y);
                if (!this.PattonExploredKb[x + 1][y] && !frontier.contains(p)) {
                   frontier.add(p);
                }
             }
             if (y > 0) {
                Point p = new Point(x, y - 1);
                if (!this.PattonExploredKb[x][y - 1] && !frontier.contains(p)) {
                   frontier.add(p);
                }
             }
             if (y < width - 1) {
                Point p = new Point(x, y + 1);
                if (!this.PattonExploredKb[x][y + 1] && !frontier.contains(p)) {
                   frontier.add(p);
                }
             }
          }
       }

       return frontier;
    }

    private boolean askPit(int x, int y) {
       int width = this.world.getPlayWidth();

       if (this.PattonPitKb[x][y] == PattonFalse) {
          return false;
       }

       if (x > 0) {
          if (this.PattonBreezeKb[x - 1][y] == this.PattonFalse) {
             return false;
          }
       }
       if (x < width - 1) {
          if (this.PattonBreezeKb[x + 1][y] == this.PattonFalse) {
             return false;
          }
       }
       if (y > 0) {
          if (this.PattonBreezeKb[x][y - 1] == this.PattonFalse) {
             return false;
          }
       }
       if (y < width - 1) {
          if (this.PattonBreezeKb[x][y + 1] == this.PattonFalse) {
             return false;
          }
       }

       return true;
    }

    private LinkedList<Point> getSuccessors(Point p) {
       int width = this.world.getPlayWidth();

       LinkedList<Point> successors = new LinkedList<Point>();
       if (p.x > 0) {
          successors.add(new Point(p.x - 1, p.y));
       }
       if (p.x < width - 1) {
          successors.add(new Point(p.x + 1, p.y));
       }
       if (p.y > 0) {
          successors.add(new Point(p.x, p.y - 1));
       }
       if (p.y < width - 1) {
          successors.add(new Point(p.x, p.y + 1));
       }

       return successors;
    }

    private boolean[][] deepCopy(boolean[][] original) {
        if (original == null) {
            return null;
        }

        boolean[][] result = new boolean[original.length][];
        for (int i = 0; i < original.length; i++) {
           result[i] = Arrays.copyOf(original[i], original[i].length);
        }
        return result;
    }

    private LinkedList<Point> aStarBiatch(Point source, Point destination, int cost, LinkedList<Point> visited) {
       if (source.equals(destination)) {
          LinkedList<Point> path = new LinkedList<Point>();
          return path;
       }

       visited.add(source);

       LinkedList<LinkedList<Point>> successorPaths = new LinkedList<LinkedList<Point>>();

       LinkedList<Point> successors = this.getSuccessors(source);
       for (Point p : successors) {
          if (visited.contains(p)) {
             continue;
          }

          LinkedList<Point> path;

          if (!this.PattonExploredKb[p.x][p.y]) {
             path = new LinkedList<Point>();
          } else {
             LinkedList<Point> visitedCopy = (LinkedList<Point>)visited.clone();
             path = aStarBiatch(p, destination, cost + 1, visitedCopy);
          }

          if (path == null) {
             continue;
          }

          path.addFirst(p);
          if (path.getLast().equals(destination)) {
             successorPaths.add(path);
          }
       }

       if (successorPaths.size() == 0) {
          return null;
       }

       LinkedList<Point> best = null;
       for (LinkedList<Point> path : successorPaths) {
          if (best == null || best.size() > path.size()) {
             best = path;
          }
       }
       return best;
    }

//     private int heuristic(Point source, Point destination) {
//        return Math.abs(source.x - destination.x) + Math.abs(source.y - destination.y);
//     }

//     private int evaluate(int cost, Point source, Point destination) {
//        return cost + this.heuristic(source, destination);
//     }

    private void setPathToPoint(Point destination) {
       int width = this.world.getPlayWidth();

       Point source = new Point(this.getX() - 1, this.getY() - 1);
       LinkedList<Point> path = aStarBiatch(source, destination, 0, new LinkedList<Point>());

       String heading = this.heading;
       for (Point p : path) {
          if (p.x < source.x) {
             if (heading.equals("NORTH")) {
                this.PattonActionQueue.add("left");
             } else if (heading.equals("SOUTH")) {
                this.PattonActionQueue.add("right");
             } else if (heading.equals("EAST")) {
                this.PattonActionQueue.add("left");
                this.PattonActionQueue.add("left");
             } else if (heading.equals("WEST")) {
             }

             heading = "WEST";
          } else if (p.x > source.x) {
             if (heading.equals("NORTH")) {
                this.PattonActionQueue.add("right");
             } else if (heading.equals("SOUTH")) {
                this.PattonActionQueue.add("left");
             } else if (heading.equals("EAST")) {
             } else if (heading.equals("WEST")) {
                this.PattonActionQueue.add("left");
                this.PattonActionQueue.add("left");
             }

             heading = "EAST";
          } else if (p.y < source.y) {
             if (heading.equals("NORTH")) {
             } else if (heading.equals("SOUTH")) {
                this.PattonActionQueue.add("left");
                this.PattonActionQueue.add("left");
             } else if (heading.equals("EAST")) {
                this.PattonActionQueue.add("left");
             } else if (heading.equals("WEST")) {
                this.PattonActionQueue.add("right");
             }

             heading = "NORTH";
          } else if (p.y > source.y) {
             if (heading.equals("NORTH")) {
                this.PattonActionQueue.add("left");
                this.PattonActionQueue.add("left");
             } else if (heading.equals("SOUTH")) {
             } else if (heading.equals("EAST")) {
                this.PattonActionQueue.add("right");
             } else if (heading.equals("WEST")) {
                this.PattonActionQueue.add("left");
             }

             heading = "SOUTH";
          }

          this.PattonActionQueue.add("move");

          source = p;
       }
    }

    /**************************************************************************************/


    public Agent(TheWumpusWorld playSpace)
    {
        int size;
        world = playSpace;
        tracking = new int[world.getPlayWidth()][world.getPlayWidth()];
        eastImage = new GreenfootImage("AgentEast.png");
        southImage = new GreenfootImage("AgentSouth.png");
        westImage = new GreenfootImage("AgentWest.png");
        northImage = new GreenfootImage("AgentNorth.png");
        size = world.getCellSize() * 9 / 10;
        eastImage.scale(size, size);
        southImage.scale(size, size);
        westImage.scale(size, size);
        northImage.scale(size, size);
        heading = "EAST";
        foreground = eastImage;
        setImage(foreground);
        isAlive = true;

        //Initialize your custom variables
        bumped = turning = false;
    }
    /**
     * Act - do whatever the Agent wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        if(isAlive == true)
            makeNextMove();

        world.score.updateImage();
    }

    private void turnLeft()
    {
        world.bump.setScore(0);
        if(heading.equals("NORTH"))
        {
            heading = "WEST";
            foreground = westImage;
        }
        else if(heading.equals("EAST"))
        {
            heading = "NORTH";
            foreground = northImage;
        }
        else if(heading.equals("SOUTH"))
        {
            heading = "EAST";
            foreground = eastImage;
        }
        else if(heading.equals("WEST"))
        {
            heading = "SOUTH";
            foreground = southImage;
        }
        setImage(foreground);
        world.score.subtract(10);
    }

    private void turnRight()
    {
        world.bump.setScore(0);
        if(heading.equals("NORTH"))
        {
            heading = "EAST";
            foreground = eastImage;
        }
        else if(heading.equals("EAST"))
        {
            heading = "SOUTH";
            foreground = southImage;
        }
        else if(heading.equals("SOUTH"))
        {
            heading = "WEST";
            foreground = westImage;
        }
        else if(heading.equals("WEST"))
        {
            heading = "NORTH";
            foreground = northImage;
        }
        setImage(foreground);
        world.score.subtract(10);
    }

    private boolean move()
    {
        world.bump.setScore(0);
        if(world.wallToThe(heading))
        {
            world.bump.setScore(1);
            world.score.subtract(20);
            return false;
        }
        else
        {
            if(heading.equals("NORTH"))
                setLocation(getX(), getY() - 1);
            else if(heading.equals("EAST"))
                setLocation(getX() + 1, getY());
            else if(heading.equals("SOUTH"))
                setLocation(getX(), getY() + 1);
            else if(heading.equals("WEST"))
                setLocation(getX() - 1, getY());
        }

        if(world.testForDeath())
            world.score.subtract(1000);
        else
            world.score.subtract(20);

        return true;
    }

    private boolean senseBreeze()
    {
        world.score.subtract(1);
        return world.testForBreeze();
    }

    private boolean senseStench()
    {
        world.score.subtract(1);
        return world.testForStench();
    }

    private boolean senseGlitter()
    {
        world.score.subtract(1);
        return world.testForGlitter();
    }

    private boolean shoot()
    {
        world.bump.setScore(0);
        if(arrowsLeft > 0)
        {
            Arrow shot = new Arrow(world, heading);
            world.addObject(shot, getX(), getY());
            world.arrows.setScore(--arrowsLeft);
            world.score.subtract(20);
            if(world.checkTrajectory(heading))
            {
                world.killWumpus();
                world.scream.setScore(1);
                return true;
            }
        }
        world.scream.setScore(0);
        return false;
    }

    public void setArrowCount(int count)
    {
        this.arrowsLeft = count;
    }
}
