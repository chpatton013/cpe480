import sys
import string
import itertools

if len(sys.argv) != 2:
   print('usage: python blocks.py <filename>')
   sys.exit(1)

filename = sys.argv[1]

def readDatShit(filename):
   f = open(filename, 'r')

   init = []
   goal = []

   phase = None

   for line in f:
      comment = string.split(line, '#')
      if len(comment) > 0:
         line = comment[0]

      line = string.strip(line)
      if line == '':
         continue

      if line == 'INIT':
         if phase == None:
            phase = 'init'
         else:
            raise Exception()
         continue
      elif line == 'GOAL':
         if phase == 'init':
            phase = 'goal'
         else:
            raise Exception()
         continue

      predicate = string.split(line, ' ')
      if predicate[0] == 'CLEAR':
         if len(predicate) != 2 or phase == 'init':
            raise Exception()
      elif predicate[0] == 'ON':
         if len(predicate) != 3:
            raise Exception()
      else:
         raise Exception()

      if phase == 'init':
         init.append((predicate[1], predicate[2]))
      elif phase == 'goal':
         if predicate[0] == 'ON':
            goal.append((predicate[0], predicate[1], predicate[2]))
         else: # CLEAR
            goal.append((predicate[0], predicate[1]))
      else:
         raise Exception()

   return (stateDatShit(init), goalDatShit(goal))

def copyState(state):
   copy = []
   for stack in state:
      copy.append(stack[:])
   return copy

def stateDatShit(constraints):
   state = []
   processed = [False] * len(constraints)

   while not all(processed):
      stack = []

      top = 'Table'
      dirty = True
   
      while dirty:
         dirty = False
         for i in xrange(len(constraints)):
            if processed[i]:
                continue

            a, b = constraints[i]
            if b == top:
               stack.append(a)
               top = a
               dirty = True
               processed[i] = True

      state.append(stack)

   return state

def goalDatShit(goal):
   stacks = []
   clears = []

   for g in goal:
      if g[0] == 'CLEAR':
         clears.append(g[1])
      else: # ON
         a = g[1]
         b = g[2]

         stackFound = False

         for i in xrange(len(stacks)):
            if stacks[i][0] == a:
               stacks[i] = [b] + stacks[i]
               stackFound = True
            elif stacks[i][-1] == b:
               stacks[i].append(a)
               stackFound = True

         if not stackFound:
            stacks.append([b, a])

   for i in xrange(len(stacks)):
      stacks[i] = [x for x in stacks[i] if x != 'Table']

   return (goal, mergeStacks(stacks), clears)

def mergeStacks(stacks):
   def overlap(s1, s2):
      for a in s1:
         for b in s2:
            if a == b:
               return True
      return False

   def mergeStack(s1, s2):
      start1 = None
      start2 = None
      for i in xrange(len(s1)):
         for j in xrange(len(s2)):
            if s1[i] == s2[j]:
               start1 = i
               start2 = j
               break
         if start1 != None:
            break

      if start1 == 0:
         return s2[:start2] + s1
      else:
          return s1[:start1] + s2

   for i in xrange(len(stacks) - 1):
      for j in xrange(i + 1, len(stacks)):
         if overlap(stacks[i], stacks[j]):
            mergedStack = mergeStack(stacks[i], stacks[j])
            stacks[i] = mergedStack
            del stacks[j]
            return mergeStacks(stacks)

   return stacks

def canMoveDatShit(state, move):
   firstFound = False
   for stack in state:
      if move[0] == stack[-1]:
         firstFound = True
         break
   if not firstFound:
      return False

   if move[1] == 'Table':
      return True

   lastFound = False
   for stack in state:
      if move[1] == stack[-1]:
         return True
   return False

def moveDatShit(state, move):
   if not canMoveDatShit(state, move):
      raise Exception()

   # Remove the block from the top of its old stack
   for i in xrange(len(state)):
      if state[i][-1] == move[0]:
         del state[i][-1]

   # Remove any empty stacks
   state = [x for x in state if x]

   # Place the block on top of its new stack
   if move[1] == 'Table':
      state.append([move[0]])
   else:
      for i in xrange(len(state)):
         if state[i][-1] == move[1]:
            state[i].append(move[0])

   return state

def moveDatShitList(state, moveList):
   for move in moveList:
      state = moveDatShit(state, move)
   return state

def stacksOnStacks(state, stacks):
   moves = []

   for stack in stacks:
      for i in xrange(1, len(stack)):
         move = (stack[i], stack[i - 1])
         state = moveDatShit(state, move)
         moves.append(move)

   return moves

def verifyDatShit(state, goal):
   def verifyOnShit(state, g):
      for stack in state:
         for i in xrange(len(stack) - 1):
            j = i + 1
            if stack[i] == g[1] and stack[j] == g[2]:
               return True
      return False

   def verifyClearShit(state, g):
      for stack in state:
         if stack[-1] == g[1]:
            return True
      return False

   satisfied = False
   for g in goal:
      if g[0] == 'ON':
         satisfied = verifyOnShit(state, g)
      else: # CLEAR
         satisfied = verifyClearShit(state, g)
      if not satisfied:
         return False
   return True

def minifyDatShit(state, goal, moves):
   initialState = copyState(state)

   moveSets = []
   moveSets.append(moves)

   for r in xrange(len(moves)):
      for combo in itertools.combinations(moves, r):
         try:
            moveSet = list(combo)
            state = moveDatShitList(copyState(initialState), moveSet)
            if verifyDatShit(state, goal):
               moveSets.append(moveSet)
         except Exception:
            pass

   return min(moveSets, key=len)

def solveDatShit(state, goal):
   def parkingLot(state):
      block = None
      for stack in state:
         if len(stack) > 1:
            block = stack[-1]
      if block == None:
         return ([], state)
      
      move = (block, 'Table')
      nextMoves, nextState = parkingLot(moveDatShit(state, move))
      return ([move] + nextMoves, nextState)

   initialState = copyState(state)

   moves, state = parkingLot(state)
   moves = moves + stacksOnStacks(state[:], goal[1])

   return minifyDatShit(initialState, goal[0], moves)

def printDatShit(moves):
   for move in moves:
      if move[1] == 'Table':
         print(str.format('MoveToTable({0})', move[0]))
      else:
         print(str.format('Move({0}, {1})', move[0], move[1]))

init, goal = readDatShit(filename)
moves = solveDatShit(init, goal)
printDatShit(moves)
