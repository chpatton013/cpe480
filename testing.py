def utility(x, y):
   return x*(y*3+(1-y)*0)+(1-x)*(y*5+(1-y)*1)

def average(l):
   return sum(l) / len(l)

steps = 10000

matrix = [[0 for x in xrange(steps)] for x in xrange(steps)]
expectation = [0 for x in xrange(steps)]

for x in xrange(steps):
   for y in xrange(steps):
      matrix[x][y] = utility(x / float(steps), y / float(steps))

   expectation[x] = average(matrix[x])

print(max(expectation))
# for i,x in enumerate(expectation):
#    print(i, x)
