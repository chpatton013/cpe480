#include "command_registry.hpp"

CommandRegistry::CommandRegistry() {}

CommandRegistry::CommandRegistry(const std::string& label, const Command& command) {
   this->_commands[label] = command;
}

CommandRegistry::CommandRegistry(const CommandMap& commands) {
   this->_commands = commands;
}

CommandRegistry::~CommandRegistry() {}

boost::optional< const CommandRegistry::Command& >
 CommandRegistry::getCommand(const std::string& label) const {
   auto itr = this->_commands.find(label);
   if (itr == this->_commands.end()) {
      return boost::optional< const Command& >();
   }

   return itr->second;
}

const CommandRegistry::CommandMap& CommandRegistry::getAllCommands() const {
   return this->_commands;
}

boost::optional< const CommandRegistry::Command >
 CommandRegistry::setCommand(const std::string& label, const Command& command) {
   auto itr = this->_commands.find(label);
   auto oldCommandOpt = this->getCommand(label);
   Command oldCommand;
   if (oldCommandOpt) {
      oldCommand = oldCommandOpt.get();
   }

   this->_commands[label] = command;

   if (oldCommandOpt) {
      return oldCommand;
   } else {
      return boost::optional< const Command >();
   }
}

boost::optional< const CommandRegistry::CommandMap >
 CommandRegistry::setCommands(const CommandMap& commands) {
   CommandMap oldCommands;
   for (auto newItr : commands) {
      auto label = newItr.first;
      auto newCommand = newItr.second;

      auto oldItr = this->_commands.find(label);
      if (oldItr != this->_commands.end()) {
         oldCommands[label] = oldItr->second;
      }

      this->_commands[label] = newCommand;
   }

   if (oldCommands.size() == 0) {
      return boost::optional< const CommandMap >();
   } else {
      return oldCommands;
   }
}

boost::optional< const CommandRegistry::Command >
 CommandRegistry::removeCommand(const std::string& label) {
   auto itr = this->_commands.find(label);
   if (itr == this->_commands.end()) {
      return boost::optional< const Command >();
   }

   auto oldCommand = itr->second;
   this->_commands.erase(itr);
   return oldCommand;
}

boost::optional< const CommandRegistry::CommandMap >
 CommandRegistry::removeCommands(const std::vector< std::string >& labels) {
   CommandMap commands;
   for (auto label : labels) {
      auto itr = this->_commands.find(label);
      if (itr == this->_commands.end()) {
         continue;
      }

      commands[label] = itr->second;
      this->_commands.erase(itr);
   }

   if (commands.size() == 0) {
      return boost::optional< const CommandMap >();
   } else {
      return commands;
   }
}
