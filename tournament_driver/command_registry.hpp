#pragma once

#ifdef COMMAND_REGISTRY_USE_TREE_MAP
   #include <map>
   #define MAP std::map
#else
   #include <unordered_map>
   #define MAP std::unordered_map
#endif

#include <string>
#include <vector>
#include <boost/optional/optional.hpp>
#include <boost/variant.hpp>

class CommandRegistry {
public:
   typedef boost::variant< int, double, const std::string > Type;
   typedef std::vector< const Type > Args;
   typedef void(*Command)(const Args&);
   typedef MAP< std::string, Command > CommandMap;

   CommandRegistry();
   CommandRegistry(const std::string& label, const Command& command);
   CommandRegistry(const CommandMap& commands);
   virtual ~CommandRegistry();

   boost::optional< const Command& > getCommand(const std::string& label) const;
   const CommandMap& getAllCommands() const;

   boost::optional< const Command > setCommand(const std::string& label, const Command& command);
   boost::optional< const CommandMap > setCommands(const CommandMap& commands);

   boost::optional< const Command > removeCommand(const std::string& label);
   boost::optional< const CommandMap > removeCommands(const std::vector< std::string >& labels);

private:
   CommandMap _commands;
};
